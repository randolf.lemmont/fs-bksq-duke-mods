Modifications for Black Square Duke
===================================

My collection of modifications and tools for the excellent [Black Square
Duke][duke] add on for Microsoft Flight Simulator.

[duke]: https://www.justflight.com/product/black-square-piston-turbine-duke-bundle


Contains
--------

- Templates for [Axis And Ohs][aao] software in `aao` subfolder.
- file modifications - see below.

[aao]: https://www.axisandohs.com/


Links
-----

* [Project repository][git]
* [Downloads][dl]

[git]: https://gitlab.com/randolf.lemmont/fs-bksq-duke-mods
[dl]: https://gitlab.com/randolf.lemmont/fs-bksq-duke-mods/-/releases


File modifications
------------------

**Warning:** Modifications described below require manual editing of files
within the Black Square Duke addon folders and is intended for advanced users
who are confident in doing so. Always make a backup of any modified files. **If
you experience problems with the Duke addon, always test first with fresh
install of the original addon before contacting support.** The official support
will not be able to help you with modified installation.


### No file modifications needed

I don't currently have any working file modifications that I use with the Duke.
All previous tweaks that I made were implemented in update version 1.1 of the
Duke. Thank you Black Square for your amazing support!


### Cold and dark start on the runway

**Warning:** This modification does not work yet! It is a work in progress,
something else is needed to make runway loading work.

This modification makes the aircraft load on a runway in cold and dark state,
same as loading at a parking spot. Useful if you want to load into the
simulator on small airstrip without a parking spot.

Piston stock and Grand Duke variants.

In file

`bksq-aircraft-pistonduke\SimObjects\Airplanes\bksq-aircraft-grandduke\model.base\PistonDuke_INT.xml`

find the following line (at or around 630 lines into the file):

```
(A:SIM ON GROUND, bool) ! (A:ENG COMBUSTION:1, bool) or if{
```

and change it to (that is, insert `<!--` and `-->` at the appropriate place):

```
(A:SIM ON GROUND, bool) ! <!-- (A:ENG COMBUSTION:1, bool) or --> if{
```

Next, in both folders

`bksq-aircraft-pistonduke\SimObjects\Airplanes\bksq-aircraft-grandduke`

and

`bksq-aircraft-pistonduke\SimObjects\Airplanes\bksq-aircraft-stockduke`

rename the file `runway.FLT` to `runway.FLT.orig` and make a copy of
`apron.FLT` file and name it `runway.FLT`. Or in short, replace the contents of
`runway.FLT` file with `apron.FLT`, while keeping a backup of the original file.

Same changes should work also for the Turbine Duke variant.
