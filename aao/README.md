Axis And Ohs templates for Black Square Duke
============================================

Autopilot
---------

`Duke_AP.tmpl`

Autopilot control with Honeycomb Bravo throttles, plus CWS button on the joystick/yoke.

**Warning:** the option _Control AP Pitch with Hardware Events_ on the Duke's
tablet has to be turned **OFF** for the pitch control included in this template
to work.

Tested with piston Grand Duke variant.

Notable highlights:
- VS buttons control ATT mode, when VS is selected on the left dial, right dial
  controls pitch
- CWS button binding is now relying on the new CWS implementation in Duke 1.1
  update


Trim
-----

`Duke_Trim.tmpl`

Mechanical trim control for aileron, rudder and elevator wheel, plus electric
elevator trim operating only when the electric trim system is actually powered.

Tested with piston Grand Duke variant.


State saving
------------

`Duke_StateSave.tmpl`

Extended state saving. Allows to save additional parameters of the aircraft,
and restore them on next flight. Can be set up for automatic operation on
starting and ending the flight, or bound to a button or voice command for
on-demand operation. State is saved per livery.

**Aircraft position** is also included in each saved state, but is not restored
automatically with the rest of the parameters. Position can be restored on
demand. In the template, position restore is bound to voice command "restore
saved position".  If you don't have voice recognition set up, you can rebind it
to a button of your choice. Reposition works best within a few miles, like on
the same airport. Repositioning at a greater distance requires sim to load new
scenery and is considered experimental.

State is saved under `LorbyAxisAndOhs Files\RStateSave` folder, usually located
under your _Documents_ folder. You need to **manually create the `RStateSave`
folder** for saving to work.  It will be then saved in `.txt` files named after
the livery being used.

The template has two auto-scripts `StateAutoSave` and `StateAutoLoad` (you can
see them with menu item _Scripts_ -> _Aircraft automated scripts_ after
applying the template to current aircraft).  Auto load is executed once, 6
seconds after flight start (and will operate only once per flight even if
executed repeatedly), and auto save is executed every 10 seconds, but only
starts saving when you shut down engines after the flight.  This is a
workaround, because executing the scripts at start and end of flight by AAO
didn't work reliably.

You can remove the auto-scripts and instead bind `StateSave`, `StateLoad` (not
the `Auto` versions) and optionally `StateLoadPosition` scripts to controller
buttons or voice commands, for on-demand operation.

Since version 0.1.0, state saved on older version can be loaded in current
version.

Tested with piston Grand Duke variant.

Saved parameters:
- plane position and orientation
- EFB tablet position (not shown/hidden state)
- control locks, external covers, wheel chocks
- KX155, KNS81, KLN90B, WX, XPDR, ADF, DME on/off state and volume
- fuel valves
- COM/NAV/ADF radio frequencies
- fuel quantity
- pitch trim power switch
- pressurization controls
- magneto switches
- XPNDR code & mode
- trims
- HVAC controls, cabin air vents
- nav/strobe & beacon/recog lights
- internal lights
- cowl flap positions
- instrument settings

Scripts (in group `Duke`):
- StateSave, StateLoad - save and load current aircraft state (saves but
  doesn't load position)
- StateLoadPosition - restore aircraft position from last save
- StateAutoSave, StateAutoLoad - used as _aircraft automated scripts_ (see few
  paragraphs up for description)
